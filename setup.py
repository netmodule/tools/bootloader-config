#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name='bootloader_config',
    version_format='{tag}.{commitcount}+{gitsha}',
    setup_requires=['setuptools-git-version'],
    description='Bootloaders detection and user interface',
    author='Alexandre Bard',
    author_email='alexandre.bard@netmodule.com',
    entry_points={
        "console_scripts": [
            "bootloader-config = bootloader_config.bootloader_config:main"
        ]
    },
    packages=['bootloader_config']
)

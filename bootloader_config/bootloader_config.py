#!/usr/bin/env python3

import os
import binascii
import struct
import sys
import signal
import time

from collections import deque

UBOOT_MAX_SIZE = 2500 * 1024
UBOOT_HEADER = '>IIIIIIIBBBB32s'
UBOOT_MAGIC = b'\x27\x05\x19\x56'
BOOT_PARTITION_SIZE = 8192 * 512

CONF_PATH = '/run/bootloader/'

class ParsingException(Exception):
    pass
class InvalidChecksumException(ParsingException):
    pass

class InvalidSizeException(ParsingException):
    pass


def find_uboot_magic(data):
    return data.find(UBOOT_MAGIC)

def parse_header(data):
    header = struct.unpack(UBOOT_HEADER, data[:64])

    # Check if size makes sense
    size = header[3]
    if size < 0 or size > UBOOT_MAX_SIZE:
        raise InvalidSizeException()

    # Check header CRC
    hcrc = header[1]

    clean_hdr = data[:4] + b'\x00' * 4 + data[8:64]
    calc_hcrc = binascii.crc32(clean_hdr)

    if calc_hcrc != hcrc:
        raise InvalidChecksumException()

    return size, header[6], header[11].decode('utf-8')

def check_crc(data, crc):
    calc_crc = binascii.crc32(data)
    if crc != calc_crc:
        raise InvalidChecksumException()


def parse_uboot(data):
    size, crc, name = parse_header(data[:64])

    check_crc(data[64:64+size], crc)

    return {'name': name, 'checksum': crc}

def find_bootloaders(data, offset=0):
    match = find_uboot_magic(data)
    if match < 0:
        return deque()  # End of data

    config = None
    try:
        config = parse_uboot(data[match:match + UBOOT_MAX_SIZE])
        config['location'] = '/dev/storage0'
        config['address'] = offset + match
        config['redundant'] = False
    except ParsingException:
        pass  # Nothing todo, likely invalid data

    following_bls = find_bootloaders(data[match + 4:], offset + match + 4)
    if config:
        following_bls.appendleft(config)
    return following_bls


def find_redundancy(bootloaders):
    for i, bl1 in enumerate(bootloaders):
        for j, bl2 in enumerate(bootloaders):
            if i != j and bl1['checksum'] == bl2['checksum']:
                bootloaders[i]['redundant'] = True
                bootloaders[i]['address2'] = bl2['address']
                bootloaders.remove(bl2)

def write_config(file, label, value):
    line = (label + ':').ljust(20) + value + '\n'
    file.write(line)

def write_config_file(bootloader, i):
    os.makedirs(CONF_PATH, exist_ok=True)
    config_file = open(CONF_PATH + 'bootloader' + str(i)  + '.config', 'w')
    write_config(config_file, 'Name', bootloader['name'])
    write_config(config_file, 'Location', bootloader['location'])
    write_config(config_file, 'Redundant', 'yes' if bootloader['redundant'] else 'no')
    write_config(config_file, 'Address', hex(bootloader['address']))

    if bootloader['redundant']:
        write_config(config_file, 'Address2', hex(bootloader['address2']))

def flash(filename, offset, data):
    file = open(filename, 'wb')
    file.seek(offset)
    file.write(data)
    file.close()

def create_device(nbr):
    path = '/dev/bootloader' + str(nbr)

    if os.path.exists(path):
        os.remove(path)
    os.mkfifo(path)
    return path

def process_loop(bootloader, path):
    fifo = open(path, 'rb')
    while True:
        header = fifo.read(64)
        if len(header) == 64:
            try:
                size, crc, _ = parse_header(header)
                data = fifo.read(size)
                check_crc(data, crc)
                print("Valid bootloader, flashing")
                flash(bootloader['location'], bootloader['address'], header + data)
                if bootloader['redundant']:
                    flash(bootloader['location'], bootloader['address2'], header + data)
            except ParsingException:
                print("Invalid data")

        elif header:
            print("Invalid data")

        # Reopen file to block on read
        fifo.close()
        fifo = open(path, 'rb')

def daemonize(bootloader, nbr):
    pid = os.fork()
    if pid > 0:
        pid_file = open('/run/bootloader{}.pid'.format(nbr), 'w')
        pid_file.write(str(pid))
        pid_file.close()
        return pid

    # Child
    path = create_device(nbr)

    def signal_handler(*_):
        os.remove(path)
        sys.exit(0)

    signal.signal(signal.SIGTERM, signal_handler)
    process_loop(bootloader, path)

def daemonize_parent(childs):
    pid = os.fork()
    if pid > 0:
        pid_file = open('/run/bootloader-config.pid', 'w')
        pid_file.write(str(pid))
        pid_file.close()
        return

    def signal_handler(*_):
        for child in childs:
            os.kill(child, signal.SIGTERM)
        sys.exit(0)

    signal.signal(signal.SIGTERM, signal_handler)
    while True:
        time.sleep(10)

def main():
    storage = open('/dev/storage0', 'rb')
    data = storage.read(BOOT_PARTITION_SIZE)
    bootloaders = list(find_bootloaders(data))

    if not bootloaders:
        print("No bootloader found")
        sys.exit(-1)

    find_redundancy(bootloaders)

    # Write config files
    childs_process = []
    for i, bootloader in enumerate(bootloaders):
        write_config_file(bootloader, i)
        pid = daemonize(bootloader, i)
        childs_process.append(pid)

    daemonize_parent(childs_process)

if __name__ == '__main__':
    main()
